package notifyapp.kingofneurons.org.notifyaplication.presentation;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import notifyapp.kingofneurons.org.notifyaplication.di.ApplicationComponent;
import notifyapp.kingofneurons.org.notifyaplication.di.ApplicationModule;
import notifyapp.kingofneurons.org.notifyaplication.di.DaggerApplicationComponent;

/**
 * Created by Neuron on 31.07.2018.
 */

public class MyApplication extends Application {

    private static ApplicationComponent applicationComponent;
    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build());
        applicationComponent = buildComponent();

    }

    public ApplicationComponent buildComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public static Context getInstance() {
        return instance;
    }

    public static ApplicationComponent getComponent() {
        return applicationComponent;
    }
}
