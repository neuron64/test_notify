package notifyapp.kingofneurons.org.notifyaplication.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import notifyapp.kingofneurons.org.notifyaplication.R;
import notifyapp.kingofneurons.org.notifyaplication.di.DaggerMainComponent;
import notifyapp.kingofneurons.org.notifyaplication.domain.GetMessagesDB;
import notifyapp.kingofneurons.org.notifyaplication.model.Message;
import notifyapp.kingofneurons.org.notifyaplication.service.MessageJobService;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;

    @BindView(R.id.bn_stop)
    Button bnStop;

    private FirebaseJobDispatcher dispatcher;

    @Inject
    GetMessagesDB getMessagesDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        startJobService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMessagesDB.execute(this::setData,
                throwable -> Log.e("my_log", "accept: ", throwable), null);
    }

    private void setData(List<Message> data) {
        AdapterMessage adapterMessage = new AdapterMessage();
        adapterMessage.setDate(data);
        rvContent.setLayoutManager(new LinearLayoutManager(this));
        rvContent.setAdapter(adapterMessage);
    }

    private void startJobService(){
        Job myJob = dispatcher.newJobBuilder()
                .setService(MessageJobService.class)
                .setTag(MessageJobService.TAG)
                .setTrigger(Trigger.executionWindow(60 ,60 * 2))
                .setReplaceCurrent(true)
                .setRecurring(true)
                /*.setLifetime(Lifetime.FOREVER | Lifetime.UNTIL_NEXT_BOOT)*/
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .build();

        dispatcher.mustSchedule(myJob);
    }

    private void stopJobService(){
        dispatcher.cancel(MessageJobService.TAG);
    }

    @OnClick(R.id.bn_stop)
    public void onClick(){
        stopJobService();
    }

    public void inject() {
        DaggerMainComponent.builder()
                .applicationComponent(MyApplication.getComponent())
                .build()
                .inject(this);
    }
}
