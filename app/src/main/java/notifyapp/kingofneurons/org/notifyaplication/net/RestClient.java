package notifyapp.kingofneurons.org.notifyaplication.net;

import android.content.Context;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import java.io.IOException;

import notifyapp.kingofneurons.org.notifyaplication.R;
import notifyapp.kingofneurons.org.notifyaplication.model.Message;
import notifyapp.kingofneurons.org.notifyaplication.utils.Util;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Neuron on 31.07.2018.
 */

public class RestClient {

    private final OkHttpClient client;

    private final Context context;

    public RestClient(OkHttpClient client, Context context){
        this.client = client;
        this.context = context;
    }

    private OkHttpClient getClient() {
        return client;
    }

    private HttpUrl.Builder getHttpUrlBuilder(@Nullable String... segments) {
        HttpUrl.Builder httpUrlBuilder = new HttpUrl.Builder();
        httpUrlBuilder
                .scheme("https")
                .host("disk.tsft.ru");

        if (segments != null)
            for (String segment : segments) httpUrlBuilder.addPathSegment(segment);

        return httpUrlBuilder;
    }

    private Request getRequest(HttpUrl url) {
        return new Request.Builder()
                .url(url)
                .get()
                .build();
    }

    private Response getResponse(@Nullable String... segments) throws IOException {
        return getClient().newCall(getRequest(getHttpUrlBuilder(segments).build())).execute();
    }

    private <T> T getDataFromResponse(Response response, Class<T> classOfT) throws IOException {
        return new Gson().fromJson(response.body().string(), classOfT);
    }

    public Message[] getMessage() throws IOException {
        return getMessageMock();
//        return getDataFromResponse(getResponse("vtb"), Message[].class);
    }

    //fixme: временное решение
    private Message[] getMessageMock(){
        return new Gson().fromJson(Util.inputStreamToString(context.getResources().openRawResource(R.raw.data)), Message[].class);
    }
}
