package notifyapp.kingofneurons.org.notifyaplication.domain;

import io.reactivex.Single;
import notifyapp.kingofneurons.org.notifyaplication.data.RepositoryMessage;
import notifyapp.kingofneurons.org.notifyaplication.model.Message;

/**
 * Created by Neuron on 31.07.2018.
 */

public class GetLastMessagesServer extends SingleUseCase<Message, Void>{

    private RepositoryMessage repositoryMessage;

    public GetLastMessagesServer(BaseSchedulerProvider schedulerProvider, RepositoryMessage repositoryMessage) {
        super(schedulerProvider);
        this.repositoryMessage = repositoryMessage;
    }

    @Override
    public Single<Message> buildUseCase(Void aVoid) {
        return repositoryMessage.getLastMessageFromServer();
    }

}
