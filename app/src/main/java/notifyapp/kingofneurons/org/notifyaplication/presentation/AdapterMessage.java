package notifyapp.kingofneurons.org.notifyaplication.presentation;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import notifyapp.kingofneurons.org.notifyaplication.R;
import notifyapp.kingofneurons.org.notifyaplication.model.Message;

public class AdapterMessage extends RecyclerView.Adapter{

    private List<Message> items;

    public AdapterMessage(){
        this.items = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MessageHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof MessageHolder){
            ((MessageHolder) holder).bind(at(position));
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public Message at(int position){
        return items.get(position);
    }

    public void setDate(@NonNull  List<Message> items){
        this.items.clear();
        this.items.addAll(items);
    }

    static class MessageHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_title)
        TextView title;
        @BindView(R.id.tv_text)
        TextView message;

        public MessageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Message message){
            this.title.setText(message.getText());
            this.message.setText(message.getSubject());
        }
    }
}
