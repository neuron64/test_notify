package notifyapp.kingofneurons.org.notifyaplication.net;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Neuron on 31.07.2018.
 */

public class ApiErrorInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        //TODO: add errors
        Request.Builder requestBuilder = chain.request().newBuilder();
        return chain.proceed(requestBuilder.build());
    }
}
