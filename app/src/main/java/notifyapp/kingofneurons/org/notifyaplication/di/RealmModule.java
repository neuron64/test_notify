package notifyapp.kingofneurons.org.notifyaplication.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import notifyapp.kingofneurons.org.notifyaplication.data.RepositoryRealm;

/**
 * Created by Neuron on 31.07.2018.
 */

@Module
public class RealmModule {

    @Provides @Singleton
    RepositoryRealm getRepositoryRealm(){
        return new RepositoryRealm();
    }
}
