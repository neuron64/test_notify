package notifyapp.kingofneurons.org.notifyaplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Neuron on 31.07.2018.
 */

public class ResponseMessages implements Parcelable{

    private List<Message> messages;

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.messages);
    }

    public ResponseMessages() {
    }

    protected ResponseMessages(Parcel in) {
        this.messages = in.createTypedArrayList(Message.CREATOR);
    }

    public static final Creator<ResponseMessages> CREATOR = new Creator<ResponseMessages>() {
        @Override
        public ResponseMessages createFromParcel(Parcel source) {
            return new ResponseMessages(source);
        }

        @Override
        public ResponseMessages[] newArray(int size) {
            return new ResponseMessages[size];
        }
    };
}
