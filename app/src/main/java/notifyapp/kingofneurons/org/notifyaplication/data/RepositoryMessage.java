package notifyapp.kingofneurons.org.notifyaplication.data;

import android.util.Pair;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import notifyapp.kingofneurons.org.notifyaplication.model.Message;
import notifyapp.kingofneurons.org.notifyaplication.net.RestClient;

/**
 * Created by Neuron on 31.07.2018.
 */

public class RepositoryMessage {

    //fixme: just for test
    private static int countMessage = 1;
    private final String datePattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private RepositoryRealm repositoryRealm;
    private RestClient restClient;

    public RepositoryMessage(RepositoryRealm repositoryRealm, RestClient restClient) {
        this.repositoryRealm = repositoryRealm;
        this.restClient = restClient;
    }

    public Single<List<Message>> getMessageFromDB() {
        return Single.fromCallable(() -> repositoryRealm.getMessages());
    }

    public Single<List<Message>> getMessageFromServer() {
        return Single.fromCallable(() -> restClient.getMessage())
                .map(Arrays::asList)
                .doOnSuccess(messages -> repositoryRealm.saveMessages(messages));
    }

    public Single<Message> getLastMessageFromServer() {
        return Single.fromCallable(() -> restClient.getMessage())
                .map(messages -> {
                    if (messages.length > countMessage) return Arrays.copyOf(messages, countMessage);
                    else return null;
                })
                .doOnSuccess(messages -> countMessage++)
                .doOnError(throwable -> countMessage = 1)
                .map(Arrays::asList)
                .map(messages -> new Pair<>(isLastMessageFromDB(messages.get(messages.size() - 1).getEndDateTime()), messages))
                .doOnSuccess(messages -> repositoryRealm.saveMessages(messages.second))
                .filter(booleanListPair -> booleanListPair.first)
                .toSingle()
                .map(booleanListPair -> booleanListPair.second.get(booleanListPair.second.size() - 1));
    }

    private boolean isLastMessageFromDB(Date date) {
        List<Message> messageList = repositoryRealm.getMessages();
        if (messageList != null && messageList.size() > 0) return messageList.get(messageList.size() - 1).getEndDateTime().before(date);
        else return true;
    }

}
