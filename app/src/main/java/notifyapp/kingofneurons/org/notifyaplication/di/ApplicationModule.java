package notifyapp.kingofneurons.org.notifyaplication.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import notifyapp.kingofneurons.org.notifyaplication.data.RepositoryMessage;
import notifyapp.kingofneurons.org.notifyaplication.data.RepositoryRealm;
import notifyapp.kingofneurons.org.notifyaplication.domain.BaseSchedulerProvider;
import notifyapp.kingofneurons.org.notifyaplication.domain.SchedulerProvider;
import notifyapp.kingofneurons.org.notifyaplication.net.RestClient;

/**
 * Created by Neuron on 31.07.2018.
 */

@Module
public class ApplicationModule {

    private final Context context;

    public ApplicationModule(Context context){
        this.context = context.getApplicationContext();
    }

    @Provides
    @Singleton
    public Context getContext(){
        return context;
    }

    @Provides
    @Singleton
    RepositoryMessage getUserRepository(RepositoryRealm repositoryRealm, RestClient restClient){
        return new RepositoryMessage(repositoryRealm, restClient);
    }

    @Singleton
    @Provides
    BaseSchedulerProvider baseSchedulerProvider(){
        return new SchedulerProvider();
    }
}
