package notifyapp.kingofneurons.org.notifyaplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Neuron on 31.07.2018.
 */

@RealmClass
public class Message implements Parcelable, RealmModel {

    @PrimaryKey
    private String id;

    private String subject;

    private String text;

    private Date startDateTime;

    private Date endDateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.subject);
        dest.writeString(this.text);
        dest.writeLong(this.startDateTime != null ? this.startDateTime.getTime() : -1);
        dest.writeLong(this.endDateTime != null ? this.endDateTime.getTime() : -1);
    }

    public Message() {
    }

    protected Message(Parcel in) {
        this.id = in.readString();
        this.subject = in.readString();
        this.text = in.readString();
        long tmpStartDateTime = in.readLong();
        this.startDateTime = tmpStartDateTime == -1 ? null : new Date(tmpStartDateTime);
        long tmpEndDateTime = in.readLong();
        this.endDateTime = tmpEndDateTime == -1 ? null : new Date(tmpEndDateTime);
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                ", startDateTime=" + startDateTime +
                ", endDateTime=" + endDateTime +
                '}';
    }
}
