package notifyapp.kingofneurons.org.notifyaplication.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import java.util.Random;

import javax.inject.Inject;

import notifyapp.kingofneurons.org.notifyaplication.R;
import notifyapp.kingofneurons.org.notifyaplication.di.DaggerServiceJobComponent;
import notifyapp.kingofneurons.org.notifyaplication.domain.GetLastMessagesServer;
import notifyapp.kingofneurons.org.notifyaplication.presentation.MainActivity;
import notifyapp.kingofneurons.org.notifyaplication.presentation.MyApplication;

/**
 * Created by Neuron on 31.07.2018.
 */

public class MessageJobService extends JobService {

    @Inject
    protected GetLastMessagesServer getMessageUseCase;

    public static final String TAG = "my-unique-tag";

    @Override
    public void onCreate() {
        inject();
        super.onCreate();
    }

    @Override
    public boolean onStartJob(JobParameters job) {
        getMessageUseCase.execute(messages -> showNotification(messages.getText(), messages.getSubject()),
                throwable -> Log.e(TAG, "accept: ", throwable), disposable -> {
        }, () -> jobFinished(job, true),
                null);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

    public void inject() {
        DaggerServiceJobComponent.builder()
                .applicationComponent(MyApplication.getComponent())
                .build()
                .inject(this);
    }

    private void showNotification(String title, String text){
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Random r = new Random();

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(intent);
        PendingIntent pIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channelId")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentText(text)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && notificationManager != null) {
            notificationManager.createNotificationChannel(new NotificationChannel("channelId", "main channel", NotificationManager.IMPORTANCE_DEFAULT));
        }

        if (notificationManager != null) {
            notificationManager.notify(r.nextInt(Integer.MAX_VALUE - 1), notificationBuilder.build());
        }
    }
}
