package notifyapp.kingofneurons.org.notifyaplication.di;

import dagger.Component;
import notifyapp.kingofneurons.org.notifyaplication.service.MessageJobService;

/**
 * Created by Neuron on 31.07.2018.
 */

@Presenter
@Component(dependencies = ApplicationComponent.class, modules = MainModule.class)
public interface ServiceJobComponent {

    void inject(MessageJobService messageJobService);

}
