package notifyapp.kingofneurons.org.notifyaplication.data;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import notifyapp.kingofneurons.org.notifyaplication.model.Message;

/**
 * Created by Neuron on 31.07.2018.
 */


public class RepositoryRealm {

    public List<Message> getMessages() {
        try (Realm realm = getRealm()) {
            RealmResults<Message> locationRealm = realm.where(Message.class).findAll();
            if(locationRealm != null) {
                return realm.copyFromRealm(locationRealm);
            }else{
                return null;
            }
        }
    }

    public void saveMessages(List<Message> messages) {
        try (Realm realm = getRealm()) {
            realm.executeTransaction(realm1 -> {
                if(messages != null && messages.size() > 0){
                    realm1.insertOrUpdate(messages);
                }
            });
        }
    }

    private Realm getRealm() {
        return Realm.getDefaultInstance();
    }

}