package notifyapp.kingofneurons.org.notifyaplication.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import notifyapp.kingofneurons.org.notifyaplication.BuildConfig;
import notifyapp.kingofneurons.org.notifyaplication.net.ApiErrorInterceptor;
import notifyapp.kingofneurons.org.notifyaplication.net.RestClient;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Neuron on 31.07.2018.
 */

@Module
public class NetworkModule {

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new ApiErrorInterceptor());

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }

        return builder.build();
    }


    @Provides
    @Singleton
    RestClient getRestClient(OkHttpClient client, Context context){
        return new RestClient(client, context);
    }

}
