package notifyapp.kingofneurons.org.notifyaplication.domain;

import java.util.List;

import io.reactivex.Single;
import notifyapp.kingofneurons.org.notifyaplication.data.RepositoryMessage;
import notifyapp.kingofneurons.org.notifyaplication.model.Message;

/**
 * Created by Neuron on 31.07.2018.
 */

public class GetMessagesDB extends SingleUseCase<List<Message>, Void>{

    private RepositoryMessage repositoryMessage;

    public GetMessagesDB(BaseSchedulerProvider schedulerProvider, RepositoryMessage repositoryMessage) {
        super(schedulerProvider);
        this.repositoryMessage = repositoryMessage;
    }

    @Override
    public Single<List<Message>> buildUseCase(Void aVoid) {
        return repositoryMessage.getMessageFromDB();
    }
}
