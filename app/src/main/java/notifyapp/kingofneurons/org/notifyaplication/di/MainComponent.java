package notifyapp.kingofneurons.org.notifyaplication.di;

import dagger.Component;
import notifyapp.kingofneurons.org.notifyaplication.presentation.MainActivity;

@Presenter
@Component(dependencies = ApplicationComponent.class, modules = MainModule.class)
public interface MainComponent {

    void inject(MainActivity mainActivity);

}
