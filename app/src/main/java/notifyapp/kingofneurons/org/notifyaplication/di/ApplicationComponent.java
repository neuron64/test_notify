package notifyapp.kingofneurons.org.notifyaplication.di;

import javax.inject.Singleton;

import dagger.Component;
import notifyapp.kingofneurons.org.notifyaplication.data.RepositoryMessage;
import notifyapp.kingofneurons.org.notifyaplication.data.RepositoryRealm;
import notifyapp.kingofneurons.org.notifyaplication.domain.BaseSchedulerProvider;

/**
 * Created by Neuron on 31.07.2018.
 */

@Singleton
@Component(modules = {ApplicationModule.class, RealmModule.class, NetworkModule.class})
public interface ApplicationComponent {

    BaseSchedulerProvider scheduler();

    RepositoryRealm repositoryRealm();

    RepositoryMessage repositoryMessage();
}