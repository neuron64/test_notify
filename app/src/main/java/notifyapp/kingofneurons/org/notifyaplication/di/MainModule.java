package notifyapp.kingofneurons.org.notifyaplication.di;

import dagger.Module;
import dagger.Provides;
import notifyapp.kingofneurons.org.notifyaplication.data.RepositoryMessage;
import notifyapp.kingofneurons.org.notifyaplication.domain.BaseSchedulerProvider;
import notifyapp.kingofneurons.org.notifyaplication.domain.GetLastMessagesServer;
import notifyapp.kingofneurons.org.notifyaplication.domain.GetMessagesDB;

/**
 * Created by Neuron on 31.07.2018.
 */

@Module
public class MainModule {

    @Presenter @Provides
    GetMessagesDB getMessageDBUseCase(BaseSchedulerProvider schedulerProvider, RepositoryMessage repositoryMessage){
        return new GetMessagesDB(schedulerProvider, repositoryMessage);
    }

    @Presenter @Provides
    GetLastMessagesServer getMessageUseCase(BaseSchedulerProvider schedulerProvider, RepositoryMessage repositoryMessage){
        return new GetLastMessagesServer(schedulerProvider, repositoryMessage);
    }
}
